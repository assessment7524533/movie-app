﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Net;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MovieApp.Models;
using MovieApp.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AndroidX.AppCompat.App;

namespace MovieApp.Activities
{
    [Activity(Label = "Movie List")]
    public class MovieListActivity : AppCompatActivity
    {
        private ListView movieListView;
        private List<Movies> movies;
        private string selectedGenreId;

        private int currentPage = 1;
        private bool isLoadingData = false;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_movie_list);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);

            movieListView = FindViewById<ListView>(Resource.Id.movieListView);

            selectedGenreId = Intent.GetStringExtra("SelectedGenreId");

            Console.WriteLine("selectedGenreId: {0}", selectedGenreId);

            var apiService = new MovieApiService();
            movies = await apiService.GetMoviesByGenreAsync(selectedGenreId, currentPage);

            if (movies != null && movies.Count > 0)
            { 
                movieListView.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, movies.Select(g => g.Title).ToList());
                movieListView.ItemClick += MovieListView_ItemClick;
                movieListView.Scroll += MovieListView_Scroll;
            }
            else
            {
                // Handle the case where no movies were fetched
                Toast.MakeText(this, "No movies available.", ToastLength.Long).Show();
            }
        }

        [Obsolete]
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                // Handle the back button click
                OnBackPressed();
                return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void MovieListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (e.Position >= 0 && e.Position < movies.Count)
            {
                var selectedMovie= movies[e.Position];
                var intent = new Intent(this, typeof(MovieDetailsActivity));
                intent.PutExtra("SelectedMovieId", selectedMovie.Id);

                StartActivity(intent);
            }
            else
            {
                Toast.MakeText(this, "Please select a genre.", ToastLength.Short).Show();
            }
        }

        // Add the MovieListView_Scroll event handler
        private async void MovieListView_Scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            // Check if the last item is visible and if data is not currently being loaded
            if (e.FirstVisibleItem + e.VisibleItemCount == e.TotalItemCount && !isLoadingData)
            {
                isLoadingData = true;

                // Increment the page number and fetch more movies
                currentPage++;
                var apiService = new MovieApiService();
                var newMovies = await apiService.GetMoviesByGenreAsync(selectedGenreId, currentPage);

                if (newMovies != null && newMovies.Count > 0)
                {
                    movies.AddRange(newMovies);
                    (movieListView.Adapter as ArrayAdapter<string>).AddAll(newMovies.Select(g => g.Title).ToList());
                    (movieListView.Adapter as ArrayAdapter<string>).NotifyDataSetChanged();
                }

                isLoadingData = false;
            }
        }
    }
}