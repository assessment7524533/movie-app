﻿using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MovieApp.Models;
using MovieApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AndroidX.AppCompat.App;

namespace MovieApp.Activities
{
    [Activity(Label = "Genre List")]
    public class GenreListActivity : AppCompatActivity
    {
        private ListView genreListView;
        private List<Genre> genres;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            SetContentView(Resource.Layout.activity_genre_list);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);

            genreListView = FindViewById<ListView>(Resource.Id.genreListView);

            var apiService = new MovieApiService();
            genres = await apiService.GetGenresAsync();

            genreListView.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, genres.Select(g => g.Name).ToList());

            genreListView.ItemClick += GenreListView_ItemClick;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                // Handle the back button click
                OnBackPressed();
                return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void GenreListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (e.Position >= 0 && e.Position < genres.Count)
            {
                var selectedGenre = genres[e.Position];
                var intent = new Intent(this, typeof(MovieListActivity));
                intent.PutExtra("SelectedGenreId", selectedGenre.Id.ToString());

                StartActivity(intent);
            }
            else
            {
                Toast.MakeText(this, "Please select a genre.", ToastLength.Short).Show();
            }
        }
    }
}