﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.AppCompat.App;
using MovieApp.Adapters;
using MovieApp.Helper;
using MovieApp.Models;
using MovieApp.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MovieApp.Activities
{
    [Activity(Label = "Movie Details")]
    public class MovieDetailsActivity : AppCompatActivity
    {
        private ListView userReviewsListView;
        private List<UserReview> userReviews;

        private ImageView posterImageView;
        private TextView titleTextView;
        private TextView overviewTextView;
        private TextView releaseDateTextView;

        private UserReviewAdapter userReviewAdapter;

        private int currentPage = 1;
        private bool isLoadingData = false;

        private int movieId;

        private DatabaseHelper _dbHelper;
        private Button _favoriteButton;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_movie_detail);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);

            posterImageView = FindViewById<ImageView>(Resource.Id.posterImageView);
            titleTextView = FindViewById<TextView>(Resource.Id.titleTextView);
            overviewTextView = FindViewById<TextView>(Resource.Id.overviewTextView);
            releaseDateTextView = FindViewById<TextView>(Resource.Id.releaseDateTextView);

            movieId = Intent.GetIntExtra("SelectedMovieId", 0);

            _dbHelper = new DatabaseHelper(System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "favorites.db"));

            _favoriteButton = FindViewById<Button>(Resource.Id.favoriteButton);
            _favoriteButton.Click += FavoriteButton_Click;

            UpdateFavoriteButtonState();

            Console.WriteLine("movieId : {0}", movieId);

            userReviewsListView = FindViewById<ListView>(Resource.Id.userReviewsListView);

            if (movieId != 0)
            {
                var apiService = new MovieApiService();
                MovieDetails movieDetails = await apiService.GetMovieDetailsAsync(movieId);

                if (movieDetails != null)
                {
                    /*string jsonString = JsonConvert.SerializeObject(movieDetails, Formatting.Indented);
                    Console.WriteLine(jsonString);*/

                    // Load movie details into the UI components
                    titleTextView.Text = movieDetails.Title;
                    overviewTextView.Text = movieDetails.Overview;
                    releaseDateTextView.Text = $"Release Date: {movieDetails.ReleaseDate}";

                    if (!string.IsNullOrEmpty(movieDetails.PosterPath))
                    {
                        LoadImageFromUrlAsync(movieDetails.PosterPath);
                    }
                }
                else
                {
                    Toast.MakeText(this, "No movie details data.", ToastLength.Long).Show();
                }

                await LoadUserReviewsAsync(movieId);
            }
            else
            {
                // Handle the case where movieId is missing
            }
        }

        [Obsolete]
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                // Handle the back button click
                OnBackPressed();
                return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void UpdateFavoriteButtonState()
        {
            if (_dbHelper.IsMovieFavorite(movieId))
            {
                _favoriteButton.Text = "Remove from Favorites";
            }
            else
            {
                _favoriteButton.Text = "Add to Favorites";
            }
        }

        private void FavoriteButton_Click(object sender, EventArgs e)
        {
            if (_dbHelper.IsMovieFavorite(movieId))
            {
                _dbHelper.RemoveFavoriteMovie(movieId);
            }
            else
            {
                var movie = new FavoriteMovie
                {
                    MovieId = movieId,
                    Title = titleTextView.Text,
                    PosterPath = "poster_path_here"
                    // Set other fields
                };
                _dbHelper.AddFavoriteMovie(movie);
            }

            UpdateFavoriteButtonState();
        }

        private async Task LoadImageFromUrlAsync(string imageUrl)
        {
            try
            {
                using var webClient = new WebClient();
                byte[] imageBytes = await webClient.DownloadDataTaskAsync(imageUrl);
                Bitmap bitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);

                // Set the loaded image to the ImageView
                posterImageView.SetImageBitmap(bitmap);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading image: {ex.Message}");
            }
        }

        private async Task LoadUserReviewsAsync(int movieId)
        {
            var apiService = new MovieApiService();
            UserReviewResponse userReviewResponse = await apiService.GetMovieUserReviewsAsync(movieId, currentPage);

            if (userReviewResponse != null)
            {
                userReviews = userReviewResponse.Results;
                userReviewAdapter = new UserReviewAdapter(this, userReviews);
                await Console.Out.WriteLineAsync($"userReviews: {userReviews.Count}");
                userReviewsListView.Adapter = userReviewAdapter;

                userReviewsListView.Scroll += UserReviewsListView_Scroll;
            }
            else
            {
                // Handle the case where user reviews couldn't be fetched
            }
        }

        private async void UserReviewsListView_Scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            if (e.FirstVisibleItem + e.VisibleItemCount == e.TotalItemCount && !isLoadingData)
            {
                isLoadingData = true;

                currentPage++;
                var apiService = new MovieApiService();
                var newUserReviews = await apiService.GetMovieUserReviewsAsync(movieId, currentPage);

                if (newUserReviews != null && newUserReviews.Results.Count > 0)
                {
                    foreach (var review in newUserReviews.Results)
                    {
                        userReviews.Add(review);
                    }

                    userReviewAdapter.NotifyDataSetChanged();
                }

                isLoadingData = false;
            }
        }
    }
}