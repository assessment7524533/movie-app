﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;
using MovieApp.Models;

namespace MovieApp.Helper
{
    public class DatabaseHelper
    {
        private SQLiteConnection _database;

        public DatabaseHelper(string dbPath)
        {
            _database = new SQLiteConnection(dbPath);
            _database.CreateTable<FavoriteMovie>();
        }

        public List<FavoriteMovie> GetFavoriteMovies()
        {
            return _database.Table<FavoriteMovie>().ToList();
        }

        public bool IsMovieFavorite(int movieId)
        {
            return _database.Table<FavoriteMovie>().Any(m => m.MovieId == movieId);
        }

        public int AddFavoriteMovie(FavoriteMovie movie)
        {
            return _database.Insert(movie);
        }

        public int RemoveFavoriteMovie(int movieId)
        {
            return _database.Delete<FavoriteMovie>(movieId);
        }
    }
}