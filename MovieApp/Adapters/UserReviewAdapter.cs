﻿using Android.Content;
using Android.Views;
using Android.Widget;
using MovieApp.Models;
using System.Collections.Generic;

namespace MovieApp.Adapters
{
    public class UserReviewAdapter : BaseAdapter<UserReview>
    {
        private readonly Context _context;
        private readonly List<UserReview> _userReviews;

        public UserReviewAdapter(Context context, List<UserReview> userReviews)
        {
            _context = context;
            _userReviews = userReviews;
        }

        public override int Count => _userReviews.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override UserReview this[int position] => _userReviews[position];

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            view ??= LayoutInflater.From(_context).Inflate(Resource.Layout.item_user_review, parent, false);

            UserReview userReview = _userReviews[position];

            TextView authorTextView = view.FindViewById<TextView>(Resource.Id.reviewAuthorTextView);
            TextView contentTextView = view.FindViewById<TextView>(Resource.Id.reviewContentTextView);

            authorTextView.Text = userReview.Author;
            contentTextView.Text = userReview.Content;

            return view;
        }
    }
}
