﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MovieApp.Models
{
    public class UserReview
    {
        [JsonProperty("author")]
        public string Author { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("author_details")]
        public AuthorDetails AuthorDetails { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class AuthorDetails
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("avatar_path")]
        public string AvatarPath { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)] // Allow for null values
        public float? Rating { get; set; } // Use float? to allow null values
    }

    public class UserReviewResponse
    {
        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("results")]
        public List<UserReview> Results { get; set; }

        [JsonProperty("total_pages")]
        public int TotalPages { get; set; }

        [JsonProperty("total_results")]
        public int TotalResults { get; set; }

        public static implicit operator List<object>(UserReviewResponse v)
        {
            throw new NotImplementedException();
        }
    }
}