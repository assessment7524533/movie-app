﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MovieApp.Models
{
    public class MovieTrailer
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Site { get; set; }
        public string Type { get; set; }
    }

    public class MovieTrailerResponse
    {
        public int Id { get; set; }
        public List<MovieTrailer> Results { get; set; }
    }
}