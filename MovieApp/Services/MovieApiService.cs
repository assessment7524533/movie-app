﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MovieApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.Services
{
    public class MovieApiService
    {
        private const string ApiBaseUrl = "https://api.themoviedb.org/3";
        private const string ApiKey = "74b374f58e37411d2727374a4b6605db";

        private readonly HttpClient _httpClient;

        public MovieApiService()
        {
            _httpClient = new HttpClient();
        }

        public async Task<List<Genre>> GetGenresAsync()
        {
            string endpoint = $"{ApiBaseUrl}/genre/movie/list?api_key={ApiKey}";
            HttpResponseMessage response = await _httpClient.GetAsync(endpoint);
            if (response.IsSuccessStatusCode)
            {
                string jsonResponse = await response.Content.ReadAsStringAsync();
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<GenreResponse>(jsonResponse);

                return result.Genres;
            }
            else
            {
                // Handle error case
                throw new Exception("Failed to fetch genres from API");
            }
        }

        public async Task<List<Movies>> GetMoviesByGenreAsync(string genreId, int page)
        {

            string endpoint = $"{ApiBaseUrl}/discover/movie?api_key={ApiKey}&with_genres={genreId}&page={page}";
            HttpResponseMessage response = await _httpClient.GetAsync(endpoint);

            if (response.IsSuccessStatusCode)
            {
                string jsonResponse = await response.Content.ReadAsStringAsync();
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<MovieResponse>(jsonResponse);

                return result.Results;
            }
            else
            {
                // Handle error case
                throw new Exception("Failed to fetch genres from API");
            }
        }

        public async Task<MovieDetails> GetMovieDetailsAsync(int movieId)
        {
            string endpoint = $"{ApiBaseUrl}/movie/{movieId}?api_key={ApiKey}";
            HttpResponseMessage response = await _httpClient.GetAsync(endpoint);

            if (response.IsSuccessStatusCode)
            {
                string jsonResponse = await response.Content.ReadAsStringAsync();
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<MovieDetails>(jsonResponse);

                return result;
            }
            else
            {
                // Handle error case
                throw new Exception("Failed to fetch movie details from API");
            }
        }

        public async Task<UserReviewResponse> GetMovieUserReviewsAsync(int movieId, int page)
        {
            string endpoint = $"{ApiBaseUrl}/movie/{movieId}/reviews?api_key={ApiKey}&page={page}";

            HttpResponseMessage response = await _httpClient.GetAsync(endpoint);

            if (response.IsSuccessStatusCode)
            {
                string jsonResponse = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<UserReviewResponse>(jsonResponse);

                return result;
            }
            else
            {
                // Handle error case
                throw new Exception("Failed to fetch user reviews from API");
            }
        }

        public async Task<List<MovieTrailer>> GetMovieTrailersAsync(int movieId)
        {
            string endpoint = $"{ApiBaseUrl}/movie/{movieId}/videos?api_key={ApiKey}";

            HttpResponseMessage response = await _httpClient.GetAsync(endpoint);

            if (response.IsSuccessStatusCode)
            {
                string jsonResponse = await response.Content.ReadAsStringAsync();
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<MovieTrailerResponse>(jsonResponse);

                return result.Results;
            }
            else
            {
                // Handle error case
                throw new Exception("Failed to fetch movie trailers from API");
            }
        }

        // Implement other API methods as needed
    }

    public class GenreResponse
    {
        public List<Genre> Genres { get; set; }
    }

    public class MovieResponse
    {
        public int Page { get; set; }
        public List<Movies> Results { get; set; }
    }
}